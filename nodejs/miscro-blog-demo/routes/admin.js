var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { 
    layout: 'shared/adminLayout',
    title: 'Admin Express1' });
});

/* GET blog detail page. */
router.get('/blog/detail', function(req, res, next) {
  res.render('blog/detail', { 
    layout: 'shared/adminLayout',
    title: 'Admin blog cc' });
});

router.get("/now", function(req, res, next){
  res.send('Admin now date is ' + new Date().toString());
});

module.exports = router;
