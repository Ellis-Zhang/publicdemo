var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { 
    layout: 'shared/basicLayout',
    title: 'Basic Express1' });
});

/* GET blog detail page. */
router.get('/blog/detail', function (req, res, next) {
  res.render('blog/detail', {
    layout: 'shared/basicLayout',
    title: 'Basic blog cc'
  });
});

router.get("/now", function (req, res, next) {
  res.send('Basic: now date is ' + new Date().toString());
});

module.exports = router;
