var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: '微博' });
});

/* GET login page. */
router.get('/login', function(req, res, next) {
  res.render('login', { title: '登录' });
});

/* GET reg page. */
router.get('/reg', function(req, res, next) {
  res.render('reg', { title: '注册' });
});

/* GET blog detail page. */
router.get('/blog/detail', function(req, res, next) {
  res.render('blog/detail', { title: 'blog cc' });
});

router.get("/now", function(req, res, next){
  res.send('now date is ' + new Date().toString());
});

module.exports = router;
